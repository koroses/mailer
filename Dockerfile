FROM centos:7.6.1810
MAINTAINER fahrinoer@gmail.com
USER root
COPY run.sh .
RUN yum update -y

RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;

RUN yum install rsyslog postfix dovecot dovecot-mysql -y

RUN systemctl enable postfix.service

RUN systemctl enable dovecot.service

RUN systemctl enable rsyslog

RUN mkdir -p /var/mail/vhosts/mylabzolution.com

RUN groupadd -g 5000 vmail && useradd -g vmail -u 5000 vmail -d /var/mail/ \
&& chown -R vmail:vmail /var/mail/ && chmod -R o-rwx /etc/postfix \
&& chown -R vmail:dovecot /etc/dovecot && chmod -R o-rwx /etc/dovecot

VOLUME /run /tmp

EXPOSE 25
EXPOSE 465
EXPOSE 587
EXPOSE 993
EXPOSE 995

ENTRYPOINT exec /usr/sbin/init 

CMD ["/bin/bash", "-c", "/run.sh"]
