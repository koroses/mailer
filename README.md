# Decription

POP3, IMAP, SMTP mailer. With postfix and dovecot served on Container.
Secured with SSL/TLS Protocol, and Data stored on MariaDB.

Base image: CentOS 7.6

Shoutout to Linode who bring all of this awesome configuration and working as expected.
https://www.linode.com/docs/guides/email-with-postfix-dovecot-and-mariadb-on-centos-7

